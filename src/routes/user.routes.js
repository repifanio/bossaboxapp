const { Router } = require('express')
const userController = require('../controllers/UserController')
const authMiddleware = require('../middlewares/auth')

const userRoutes = Router()

userRoutes.post('/create', userController.store)
userRoutes.get('/list', userController.list)

userRoutes.use(authMiddleware)
userRoutes.put('/update/:idUser', userController.update)
userRoutes.put('/active/:idUser', userController.active)
userRoutes.put('/inative/:idUser', userController.inactive)

module.exports = userRoutes
