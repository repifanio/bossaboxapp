const { Router } = require('express')
const toolsController = require('../controllers/ToolsController')
const authMiddleware = require('../middlewares/auth')

const userRoutes = Router()

userRoutes.get('/', toolsController.search)

userRoutes.use(authMiddleware)
userRoutes.post('/', toolsController.store)
userRoutes.delete('/:toolId', toolsController.delete)

module.exports = userRoutes
