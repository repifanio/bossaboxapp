const { Router } = require('express')
const auth = require('./auth.routes')
const users = require('./user.routes')
const tools = require('./tools.routes')

const routes = Router()

routes.use('/auth', auth)
routes.use('/user', users)
routes.use('/tools', tools)

module.exports = routes
