const { Router } = require('express')
const sessionController = require('../controllers/SessionController')

const authRoutes = Router()

authRoutes.post('/login', sessionController.login)

module.exports = authRoutes
