const createToolServie = require('../services/createToolService')
const removeToolService = require('../services/removeToolService')
const searchToolService = require('../services/searchToolService')

class ToolsController {
  async store (req, res) {
    try {
      const { title, link, description, tags, userId } = req.body

      const tool = await createToolServie.run(title, link, description, tags, userId)

      return res.status(201).json(tool)
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }

  async delete (req, res) {
    try {
      const { toolId } = req.params

      await removeToolService.run(toolId)

      return res.status(204).send()
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }

  async search (req, res) {
    try {
      const { tag = '', page = 1, perPage = 4 } = req.query

      const tools = await searchToolService.run(tag, page, perPage)

      return res.status(200).json(tools)
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }
}

module.exports = new ToolsController()
