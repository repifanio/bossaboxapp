const createUserService = require('../services/createUserService')
const updateUserService = require('../services/updateUserService')
const activeUserService = require('../services/activeUserService')
const inactiveUserService = require('../services/inactiveUserService')
const listUsersServices = require('../services/listUsersService')

class UserController {
  async store (req, res) {
    try {
      const { name, username, email, password } = req.body

      const ret = await createUserService.run(name, username, email, password)

      return res.json(ret)
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }

  async update (req, res) {
    try {
      const { idUser } = req.params
      const { name, email, password, confirmPassword } = req.body

      const ret = await updateUserService.run(idUser, name, email, password, confirmPassword)

      return res.json(ret)
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }

  async active (req, res) {
    try {
      const { idUser } = req.params

      const user = await activeUserService.run(idUser)

      return res.json(user)
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }

  async inactive (req, res) {
    try {
      const { idUser } = req.params

      const user = await inactiveUserService.run(idUser)

      return res.json(user)
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }

  async list (req, res) {
    try {
      const { page = 1, perPage = 4 } = req.query
      const users = await listUsersServices.run(page, perPage)

      return res.json(users)
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }
}

module.exports = new UserController()
