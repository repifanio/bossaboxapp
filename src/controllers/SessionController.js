const loginService = require('../services/loginService')

class SessionController {
  async login (req, res) {
    try {
      const { username, password } = req.body

      const login = await loginService.run(username, password)

      return res.json(login)
    } catch (err) {
      return res.status(400).json({ error: err.message })
    }
  }
}

module.exports = new SessionController()
