const { Sequelize, Model } = require('sequelize')

class Tool extends Model {
  static init (sequelize) {
    super.init(
      {
        title: Sequelize.STRING,
        link: Sequelize.STRING,
        description: Sequelize.STRING,
        tags: Sequelize.STRING,
        is_deleted: Sequelize.BOOLEAN
      },
      {
        sequelize,
        tableName: 'tools'
      }
    )
    return this
  }

  static associate (models) {
    this.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user'
    })
  }
}

module.exports = Tool
