const jwt = require('jsonwebtoken')
const { promisify } = require('util')

module.exports =
  async (req, res, next) => {
    const authHeader = req.headers.authorization

    if (!authHeader) {
      return res.status(401).json({ error: 'Token de autorização não enviado' })
    }

    const [, token] = authHeader.split(' ')

    try {
      const decode = await promisify(jwt.verify)(token, process.env.SECRET_JWT)
      req.userId = decode.id

      return next()
    } catch (err) {
      return res.status(401).json({ error: 'Token inválido' })
    }
  }
