const User = require('../models/User')

class CreateUserService {
  async run (name, username, email, password) {
    const user = await User.create({
      name, username, email, password: password
    })

    return user
  }
}

module.exports = new CreateUserService()
