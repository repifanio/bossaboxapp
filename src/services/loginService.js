const jwt = require('jsonwebtoken')
const User = require('../models/User')

class LoginService {
  async run (username, password) {
    const user = await User.findOne({ where: { username } })

    if (!user) throw Error('Usuário não encontrado')

    if (!(await user.checkPassword(password))) throw Error('Senha do usuário incorreta')

    const { id, name, email } = user

    const ret = {
      user: {
        id, username, name, email
      },
      token: jwt.sign({ id }, process.env.SECRET_JWT, { expiresIn: process.env.EXPIRE_JWT })
    }

    return ret
  }
}

module.exports = new LoginService()
