const Tool = require('../models/Tool')

class CreateToolService {
  async run (title, link, description, tags, userId) {
    const tagsJoined = tags.join(' %-¬ ')

    const tool = await Tool.create({ title, link, description, tags: tagsJoined, user_id: userId })

    const tagsUnjoined = tool.tags.split(' %-¬ ')

    const ret = {
      title: tool.title,
      link: tool.link,
      description: tool.description,
      tags: tagsUnjoined,
      id: tool.id
    }

    return ret
  }
}

module.exports = new CreateToolService()
