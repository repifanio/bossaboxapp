const User = require('../models/User')

class UpdateUserService {
  async run (idUser, name, email, password, confirmPassword) {
    if (password !== confirmPassword) throw Error('A senha e confirmação de senha não estão iguais')

    const user = await User.findByPk(idUser)
    if (!user) throw Error('Usuário não encontrado')

    const userUpdated = user.update({ name, email, password })

    const ret = {
      id: (await userUpdated).getDataValue('id'),
      username: (await userUpdated).getDataValue('username'),
      name: (await userUpdated).getDataValue('name'),
      email: (await userUpdated).getDataValue('email'),
      updated: (await userUpdated).getDataValue('createdAt')
    }

    return ret
  }
}

module.exports = new UpdateUserService()
