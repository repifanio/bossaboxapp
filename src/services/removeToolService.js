const Tool = require('../models/Tool')

class RemoveToolService {
  async run (toolId) {
    const tool = await Tool.findByPk(toolId)
    if (!tool) throw Error('Ferramenta não encontrada')

    await tool.destroy()
  }
}

module.exports = new RemoveToolService()
