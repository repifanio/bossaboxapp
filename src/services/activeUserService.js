const User = require('../models/User')

class ActiveUserService {
  async run (idUser) {
    const user = await User.findByPk(idUser)
    if (!user) throw Error('Usuário não encontrado')

    const userActivated = await user.update({ is_deleted: false })

    return userActivated
  }
}

module.exports = new ActiveUserService()
