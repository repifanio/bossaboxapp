const User = require('../models/User')

class ListUsersService {
  async run (page, perPage) {
    const users = await User.findAndCountAll(
      {
        where:
        {
          is_deleted: false
        },
        limit: +perPage,
        offset: (+page - 1) * +page
      }
    )
    if (!users) throw Error('Nenhum usuário encontrado')

    const allUsers = users.rows.map(user => {
      const userListed = {
        id: user.id,
        username: user.username,
        name: user.name,
        email: user.email,
        is_deleted: user.is_deleted,
        updated: user.updatedAt,
        created: user.createdAt
      }

      return userListed
    })

    const ret = {
      users: allUsers,
      pageInfo: {
        count: users.count,
        total_page: Math.ceil(users.count / +perPage)
      }
    }

    return ret
  }
}

module.exports = new ListUsersService()
