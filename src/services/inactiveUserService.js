const User = require('../models/User')

class InactiveUserService {
  async run (idUser) {
    const user = await User.findByPk(idUser)
    if (!user) throw Error('Usuário não encontrado')

    const userActivated = await user.update({ is_deleted: true })

    return userActivated
  }
}

module.exports = new InactiveUserService()
