const { Op } = require('sequelize')
const Tool = require('../models/Tool')

class SearchToolService {
  async run (tag, page, perPage) {
    const searchTools = await Tool.findAndCountAll({
      where: {
        [Op.and]: {
          tags: {
            [Op.like]: `%${tag}%`
          },
          is_deleted: false
        }
      },
      limit: +perPage,
      offset: (+page - 1) * +perPage
    })

    const allToolsFinded = searchTools.rows.map(tool => {
      const tagsUnjoined = tool.tags.split(' %-¬ ')

      const toolFinded = {
        tool: {
          id: tool.id,
          title: tool.title,
          link: tool.link,
          description: tool.description,
          tags: tagsUnjoined
        }
      }

      return toolFinded
    })

    const ret = {
      allToolsFinded,
      pageInfo: {
        count: searchTools.count,
        total_page: Math.ceil(searchTools.count / +perPage)
      }
    }

    return ret
  }
}

module.exports = new SearchToolService()
