const Sequelize = require('sequelize')
const config = require('../config/database')
const User = require('../models/User')
const Tool = require('../models/Tool')

const models = [User, Tool]

class Database {
  constructor () {
    this.init()
  }

  init () {
    if (process.env.DATABASE_URL) {
      this.connection = new Sequelize(process.env.DATABASE_URL, config)
    } else {
      this.connection = new Sequelize(
        config.database,
        config.username,
        config.password,
        config
      )
    }

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models))
  }
}

module.exports = new Database()
