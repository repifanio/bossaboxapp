# BossaBox Challenge

### 📦 O que é?

É uma aplicação responsável por realizar o cadastro e manutenção de ferramentas de desenvolvimento e tags que definem a mesma para a mesma.

### ⚙️ Algumas ferramentas utilizadas

Eslint

É uma ferramenta responsável por manter o padrão do lint no código, evitando a     'despadronização' do mesmo. Nesse projeto está sendo utilizado o padrão standard.

Husky

É responsável por executar hooks do git, no projeto atual, a mesma utiliza o hook do pré-commit para chamar o lint-staged que realiza a validação do lint do código.

GitLab-CI

É a tool responsável pela execução do CI

Postman

Utilizado para realizar as requests na API, executar os testes de integração e gerar a documentação da mesma.

Heroku

Utilizado para o Deploy da aplicação URL ([https://bossa-box-app-dev.herokuapp.com](https://bossa-box-app-dev.herokuapp.com/)/)

### 🔧 Features

- **Uso de ferramentas externas que facilitam o trabalho** **⇒** Uso do Postman que auxiliou nas request, testes de integração e fluxo completo e na documentação da API.
- **Cuidados especiais com otimização, padrões, entre outros ⇒** Foi utilizado um padrão baseado em serviços permitindo uma boa escalabilidade da aplicação, rápido entendimento e manutenção simplificada.
- **Migrations ou script para configuração do banco de dados utilizado ⇒** Foram ****utilizadas migrations e seeds para configuração do banco de dados
- **Testes ⇒** Foram desenvolvidos testes dentro do postman confirmando a integração da API. Tais testes estão presentes na documentação da API
- **Autenticação e autorização ⇒** Foi utilizada autenticação JWT no projeto através da lib *jasonwebtoken*
- **Pipelines de CI/CD ⇒** Foi implementado CI/CD no projeto na qual o mesmo possui o seguinte fluxo:

     MR ⇒ Deploy na stage e testes

     Merge na master ⇒ Deploy na produção

- **Deploy em ambientes reais, utilizando serviços de cloud externos ⇒** Foi utilizado o heroku para ambos os deploys (stage e produção) assim como uma base de dados MySql como add-on do mesmo.
- **Sugestões sobre o challenge ⇒** Não sei se seria possível devido ao fluxo de cadastros do site, mas se tivesse uma forma de realizar uma challenge em conjunto (back e front) poderia garantir que o dev além de hard skills também tenha soft skills, pois trabalhar em time exige bastante na maioria das vezes.

### ⚡ Executando

Para executar a aplicação basta clonar o repositório, entrar dentro da pasta recém (via console) clonada e digitar os seguintes comandos no console:

1. yarn
2. yarn sequelize db:migrate
3. yarn sequelize db:seed —seed 20200610025956-insert-admin-user.js
4. yarn run

*A api estará executando dessa forma no endereço [http://localhost:3000](http://localhost:3000) (a porta está configurada no .env).*

### 📄 Documentação

Para acessar a documentação da API basta clicar no seguinte link: 

[BossaBox Challenge](https://documenter.getpostman.com/view/11438498/SzzgAz9k?version=latest)

### ⚠️ Observações

- Poderia ter optado por criar uma tabela para tasks  e uma n p/ n entre tasks e tools, no entanto para o tamanho do projeto creio que não faria sentido e iria diminuir a performance
- o .env está no repositório porque já contém as informações de acesso ao banco e a secret do jwt. Óbviamente que se o caso fosse um projeto mais sério manteria no repositório somente um .env.example.
- Poderia ter incluído mais endpoints na aplicação com edição da tool, listagem de tool por id de usuário, no entanto não queria demorar na entrega e investi um pouco mais de tempo em processos de devops.